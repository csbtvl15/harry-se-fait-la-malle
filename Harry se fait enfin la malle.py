# encoding UTF-8

fournitures_scolaires = \
[{'Nom': 'Manuel scolaire', 'Poids': 0.55, 'Mana': 11},
{'Nom': 'Baguette magique', 'Poids': 0.085, 'Mana': 120},
{'Nom': 'Chaudron', 'Poids': 2.5, 'Mana': 2},
{'Nom': 'Boîte de fioles', 'Poids': 1.2, 'Mana': 4},
{'Nom': 'Téléscope', 'Poids': 1.9, 'Mana': 6},
{'Nom': 'Balance de cuivre', 'Poids': 1.3, 'Mana': 3},
{'Nom': 'Robe de travail', 'Poids': 0.5, 'Mana': 8},
{'Nom': 'Chapeau pointu', 'Poids': 0.7, 'Mana': 9},
{'Nom': 'Gants', 'Poids': 0.6, 'Mana': 25},
{'Nom': 'Cape', 'Poids': 1.1, 'Mana': 13}]

POIDS_MAXIMAL = 4


def remplissage_sans_optimisation(POIDS_MAX, fournitures):
    """
    Entrées
        POIDS_MAX : entier, le poids à ne pas dépasser en remplissant la malle
        fournitures : liste de dictionnaires, contient les objets disponibles
    Sortie : liste de dictionnaires, contient les objets dans la malle

    Cette fonction remplit la malle avec les objets qu'elle rencontre en
     premier dans la liste des fournitures si leur poids est acceptable.
    """

    poids_malle = 0
    fournitures_choisies = []

    # On ajoute chaque objet assez léger pour entrer dans la malle
    for objet in fournitures:
        # Puisque on traite des dictionnaires, on utilise la clé 'Poids'
        if objet['Poids'] + poids_malle <= POIDS_MAX:
            # On ajoute le dictionnaire entier pour ne pas le reconstruire
            fournitures_choisies.append(objet)
            poids_malle += objet['Poids']

    # On renvoit la liste de dictionnaires avec uniquement les objets choisis
    return fournitures_choisies


def optimisation(POIDS_MAX, fournitures, critere):
    """
    Entrées
        POIDS_MAX : entier, le poids à ne pas dépasser en remplissant la malle
        fournitures : liste de dictionnaire, contient les objets disponibles
        critère : chaîne de caractères, clé avec laquelle trier la table
    Sortie : liste de dictionnaires, contient les objets dans la malle

    Cette fonction remplit la malle avec les objets dans l'ordre d'apparition
    dans la liste des fournitures si leur poids est acceptable une fois les
    avoir trié dans l'ordre décroissant en fonction d'un critere à l'aide de
    la fonction tri()
    """
    # On trie les objets dans l'ordre décroissant
    fournitures = tri(fournitures, critere)[::-1]

    fournitures_choisies = []
    poids_malle = 0

    # Cette partie est similaire à celle du remplissage non optimisé
    for objet in fournitures:
        if poids_malle + objet['Poids'] <= POIDS_MAX:
            poids_malle += objet['Poids']
            fournitures_choisies.append(objet)
    # On renvoit la liste de dictionnaires avec uniquement les objets choisis
    return fournitures_choisies


def force_brute(max, fournitures):
    '''
    Entrées
        max : entier, poids à ne pas dépasser
        fournitures : liste de dictionnaires, fournitures d'Harry
    Sortie : liste de dictionnaires, fournitures choisies par Harry

    Bonus - Trouve la meilleure combinaison possible pour optimiser le poids
    en essayant toutes les possibilités
    Pas de résultat en fonction du mana car le résultat est déjà optimal
    avec l'algorithme <optimisation>
    '''
    def calcul_combinaisons(nombre_objets_dans_la_combinaison, iterable):
        """
        Entrées
            nombre_objets_dans_la_combinaison : entier, taille des combinaisons
            iterable : iterable, échantillon pour créer les combinaisons
        Sortie : liste, liste de toutes les combinaisons

        Cette fonction calcule toutes les combinaisons possibles d'une
        longueur donnée d'un iterable.
        """
        if not nombre_objets_dans_la_combinaison:
            return [[]]
        if not iterable:
            return []

        debut = [iterable[0]]
        suite = iterable[1:]
        combinaison = [debut + i for i in calcul_combinaisons(nombre_objets_dans_la_combinaison - 1, suite)]

        return combinaison + calcul_combinaisons(nombre_objets_dans_la_combinaison, suite)

    # meilleure_valeur est un entier contenant le meilleur poids obtenu
    # afin de le comparer
    # meilleure_combinaison est une liste contenant la meilleure combinaison
    meilleure_valeur = 0
    meilleure_combinaison = []

    somme = 0
    liste = [i['Poids'] for i in fournitures]
    for i in range(1, len(liste) + 1):
        for j in calcul_combinaisons(i, liste):
            somme = sum(j)
            if somme <= max and somme > meilleure_valeur:
                meilleure_valeur = somme
                meilleure_combinaison = j
                if somme == max:
                    break
    if meilleure_combinaison:
        return [j for j in fournitures for i in meilleure_combinaison if j['Poids'] == i]
    return


def tri(liste, critere):
    """
    Entrée
        liste : liste de dictionnaires, contient les éléments à trier
        critere : chaîne, clé des dictionnaires pour les comparaisons
    Sortie : liste de dictionnaires triés

    Cette fonction reçoit une liste de dictionnaires et la trie en fonction
    d'une des clés des dictionnaires définit par le paramètre <critere>
    grâce à l'algorithme du tri rapide
    """
    # Pas besoin de trier une liste d'un seul élément ou moins
    if len(liste) < 2:
        return liste

    # On utilise .pop() pour retirer le pivot de la liste ([-1] le ferait
    # rester dans la liste) et on économise des tours pour trier
    pivot = liste.pop()

    # On crée les deux partitions
    superieur = []
    inferieur = []

    # On remplie les partitions
    for objet in liste:
        if objet[critere] <= pivot[critere]:
            inferieur.append(objet)
        else:
            superieur.append(objet)

    # Tant que toute les partitions ne sont pas triées on les trie
    # Une fois cela fait on renvoie la liste triée
    return tri(inferieur, critere) + [pivot] + tri(superieur, critere)


def affichage(fournitures_choisies):
    """
    Entrée
        founitures_choisies : table, contient les objet à mettre dans la malle

    Cette fonction affiche les objets mis dans la malle à l'aide d'une liste
    de dictionnaires, affiche leurs noms et le poids et le mana total.
    """
    # On crée des listes d'entiers contenant les valeurs de poids
    # ou de mana des founitures_choisies et on applique la fonction sum()
    # dessus pour obtenir le total
    somme_poids = sum(i['Poids'] for i in fournitures_choisies)
    somme_mana = sum(i['Mana'] for i in fournitures_choisies)

    # On extrait les noms des objets de la liste de dictionnaires
    fournitures_choisies = [i['Nom'] for i in fournitures_choisies]

    # On affiche les résultats dans la console
    print(f"\nHarry a rempli sa malle, voici ce qu'il a choisi :\n"
          f"{fournitures_choisies}\n"
          f"Le poids total de la malle est : {somme_poids:.3f}\n"
          f"Le mana total de la malle est : {somme_mana}\n")


def ihm(message):
    """
    Entrée : message à écrire a début de la fonction

    Cette fonction permet à l'utilisateur de choisir les fonctions à exécuter
    grâce au clavier.
    """
    if message:
        print(message)

    # On empêche l'utilisateur de faire une saisie invalide
    try:
        # Il est nécessaire de slicer les fournitures_scolaires pour ne pas
        # les modifier avec la méthode .pop() dans la fonction tri()
        # c'est parce que le paramètre <liste> de la fonction tri()
        # pointerait sur la liste qui est contenue dans la variable
        # globale <fournitures_scolaires> et la méthode .pop() changerait
        # cette variable globale
        choix = int(input('( 1 = Rapide ; 2 = Optimisé en fonction du poids ;'
                          ' 3 = Optimisé en fonction du Mana ; 4 = Optimisé '
                          'en fonction du poids par force brute )\n'))

        if choix not in (1, 2, 3, 4):
            raise
        elif choix == 1:
            affichage(remplissage_sans_optimisation(POIDS_MAXIMAL, fournitures_scolaires[:]))
        elif choix == 2:
            affichage(optimisation(POIDS_MAXIMAL, fournitures_scolaires[:], 'Poids'))
        elif choix == 3:
            affichage(optimisation(POIDS_MAXIMAL, fournitures_scolaires[:], 'Mana'))
        else:
            affichage(force_brute(POIDS_MAXIMAL, fournitures_scolaires))
    except:
        ihm(f"<{choix}> n'est pas valide. Veuillez réessayer")

    if input('Voulez-vous essayer un nouveau mode de remplissage'
             '? (Y) : ').lower() == "y":
        ihm('Comment Harry va-t-il remplir sa malle cette fois-ci ?')

ihm('Bonjour et bienvenue dans notre remplisseur de malle automatique !'
    '\nComment Harry va-t-il remplir sa malle ?')

print('Au revoir et à bientôt !')
